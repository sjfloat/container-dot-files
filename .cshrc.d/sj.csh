# .cshrc

if (-r /opt/local/share/terminfo) then
    setenv TERMINFO /opt/local/share/terminfo
endif

setenv LC_CTYPE C
setenv LANGUAGE C
setenv LC_ALL C
setenv LANG en_US.UTF-8

setenv PAGER less
setenv LESS "-RiX"
setenv EDITOR vim


setenv GREP_OPTIONS '--directories=skip'
#
cd .
umask 002
#
#    useful settings 
#
set history = 1000
set notify
set ignoreeof
set complete
set list
set filec

# set this for all shells
set noclobber

set savehist
# If env var is set, use it
if ( $?HISTFILE ) then
    set histfile="$HISTFILE"
endif

set autolist
#set watch=(1 any any)
set nobeep
set rmstar
set time=1
set savehist=(10000 merge)
set histdup=prev
alias hgrep 'history | grep "^.............\!^"'
alias histfilehere 'set histfile="`pwd`"/history'
complete man 'n/*/c/'
complete which 'n/*/c/' 'n/*/a/'
complete cd 'n/*/d/'
complete b 'n/*/d/'
complete vi 'n/*/f:^*.{o,a}/'
complete mutf c@=@F:$HOME/Mail/@
complete ssh 'p/*/`gethostlist`/'
complete vi    'n/-t/`cut -f1 -d"	" tags | sort -u`/'
complete vim   'n/-t/`cut -f1 -d"	" tags | sort -u`/'
complete view  'n/-t/`cut -f1 -d"	" tags | sort -u`/'
complete gview 'n/-t/`cut -f1 -d"	" tags | sort -u`/'
complete gvim  'n/-t/`cut -f1 -d"	" tags | sort -u`/'
complete git-push 'p@1@F:.git/remotes@'
complete git-pull 'p@1@F:.git/remotes@'
#set complete=enhance
alias b pushd
alias y popd
unalias cd
set prompt="%n@%m:%c02:%h %#"
bindkey -v
set savedirs
if ( $?DIRSFILE ) then
    set dirsfile="$DIRSFILE"
endif

alias save "dirs -S; history -S"
